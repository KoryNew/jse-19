package ru.tsk.vkorenygin.tm.command.system;

import ru.tsk.vkorenygin.tm.command.AbstractCommand;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collection;

public class ArgumentsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "display list of arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<String> arguments = serviceLocator.getCommandService().getCommandArgs();
        for (final String argument : arguments) System.out.println(argument);
    }

    private void showCommandValue(final String value) {
        if (DataUtil.isEmpty(value)) return;
        System.out.println(value);
    }

}
