package ru.tsk.vkorenygin.tm.exception.empty;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty.");
    }

}
