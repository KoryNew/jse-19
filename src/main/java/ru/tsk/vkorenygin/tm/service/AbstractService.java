package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IRepository;
import ru.tsk.vkorenygin.tm.api.repository.IService;
import ru.tsk.vkorenygin.tm.entity.AbstractEntity;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public E add(final E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Override
    public void addAll(final Collection<E> collection) {
        if (DataUtil.isEmpty(collection)) return;
        repository.addAll(collection);
    }

    @Override
    public E findById(final String id) throws EmptyIdException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public E removeById(final String id) throws EmptyIdException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public E remove(E entity) {
        if (entity == null) return null;
        return repository.remove(entity);
    }

}
