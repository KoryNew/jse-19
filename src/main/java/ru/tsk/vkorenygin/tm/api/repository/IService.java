package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.api.repository.IRepository;
import ru.tsk.vkorenygin.tm.entity.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
