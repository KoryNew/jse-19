package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.entity.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    int getSize();

    List<Task> findAll(final Comparator<Task> comparator);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task changeStatusById(final String id, final Status status);

    Task changeStatusByName(final String name, final Status status);

    Task changeStatusByIndex(final Integer index, final Status status);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task bindTaskToProjectById(final String projectId, final String taskId);

    Task unbindTaskById(final String id);

    List<Task> findAllByProjectId(final String id);

    void removeAllTaskByProjectId(final String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

}
