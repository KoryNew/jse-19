package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.entity.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTasksByProjectId(final String id) throws EmptyIdException;

    Task bindTaskToProject(final String projectId, final String taskId) throws AbstractException;

    Task unbindTaskFromProject(final String projectId, final String taskId) throws AbstractException;

}
