package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.api.repository.IService;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.entity.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    void create(String name) throws EmptyNameException;

    void create(String name, String description) throws AbstractException;

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    List<Project> findAll();

    List<Project> findAll(final Comparator<Project> comparator);

    Project findByName(final String name) throws EmptyNameException;

    Project findByIndex(final Integer index) throws IncorrectIndexException;

    Project changeStatusById(final String id, final Status status) throws AbstractException;

    Project changeStatusByName(final String name, final Status status) throws AbstractException;

    Project changeStatusByIndex(final Integer index, final Status status) throws AbstractException;

    Project startById(String id) throws AbstractException;

    Project startByIndex(Integer index) throws AbstractException;

    Project startByName(String name) throws AbstractException;

    Project finishById(String id) throws AbstractException;

    Project finishByIndex(Integer index) throws AbstractException;

    Project finishByName(String name) throws AbstractException;

    Project updateById(final String id, final String name, final String description) throws AbstractException;

    Project updateByIndex(final Integer index, final String name, final String description) throws AbstractException;

    Project removeByIndex(final int index) throws IncorrectIndexException;

    Project removeByName(final String name) throws EmptyNameException;


}
