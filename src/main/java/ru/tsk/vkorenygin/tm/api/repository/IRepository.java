package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.entity.AbstractEntity;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    E add(E entity);

    void addAll(Collection<E> collection);

    E findById(String id) throws EmptyIdException;

    void clear();

    E removeById(String id) throws EmptyIdException;

    E remove(E entity);

}
