package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public boolean existsById(String id) {
        final Project project = findById(id);
        return project != null;
    }

    @Override
    public boolean existsByIndex(Integer index) {
        return index < entities.size();
    }

    @Override
    public int getSize() {
        return entities.size();
    }

    public List<Project> findAll(final Comparator<Project> comparator) {
        final List<Project> projectsSorted = new ArrayList<>(entities);
        projectsSorted.sort(comparator);
        return projectsSorted;
    }

    @Override
    public Project findByName(final String name) {
        if (DataUtil.isEmpty(name)) return null;
        for (final Project project : entities) {
            if (project == null) continue;
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project changeStatusByName(final String name, final Status status) {
        Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startById(String id) {
        Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByIndex(Integer index) {
        Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByName(String name) {
        Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project finishById(String id) {
        Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(Integer index) {
        Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(String name) {
        Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

}
