package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public boolean existsById(String id) {
        final Task task = findById(id);
        return task != null;
    }

    @Override
    public boolean existsByIndex(Integer index) {
        return index < entities.size();
    }

    @Override
    public int getSize() {
        return entities.size();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        final List<Task> taskList = new ArrayList<>(entities);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public Task findById(final String id) {
        for (Task task : entities) {
            if (id.equals(task.getId()))
                return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String name) {
        if (DataUtil.isEmpty(name)) return null;
        for (final Task task : entities) {
            if (task == null) continue;
            if (name.equals(task.getName()))
                return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task changeStatusByName(final String name, final Status status) {
        Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startById(String id) {
        Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByIndex(Integer index) {
        Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByName(String name) {
        Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task finishById(String id) {
        Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(Integer index) {
        Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(String name) {
        Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task bindTaskToProjectById(final String projectId, final String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String id) {
        final Task task = findById(id);
        task.setProjectId(null);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String id) {
        final List<Task> entitiesFound = new ArrayList<>();
        for (Task task : entities) {
            if (id.equals(task.getProjectId())) entitiesFound.add(task);
        }
        if (entitiesFound.size() == 0)
            return null;
        return entitiesFound;
    }

    @Override
    public void removeAllTaskByProjectId(final String id) {
        List<Task> entitiesNew = new ArrayList<>();
        for (final Task task : entities) {
            if (!id.equals(task.getProjectId())) entitiesNew.add(task);
        }
        entities = entitiesNew;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        entities.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        entities.remove(task);
        return task;
    }

}
