package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.IUserRepository;
import ru.tsk.vkorenygin.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (final User user : entities) {
            if (user == null) continue;
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user : entities) {
            if (user == null) continue;
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}
